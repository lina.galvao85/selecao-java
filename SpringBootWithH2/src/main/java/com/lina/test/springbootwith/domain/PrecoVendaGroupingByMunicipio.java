/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.domain;

import java.math.BigDecimal;

/**
 *
 * @author Lina
 */
public interface PrecoVendaGroupingByMunicipio {
    public BigDecimal getValorVenda() ;
    public String getMunicipio();
    
}
