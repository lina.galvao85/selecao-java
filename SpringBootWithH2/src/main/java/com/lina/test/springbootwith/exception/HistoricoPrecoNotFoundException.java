/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.exception;

/**
 *
 * @author Lina
 */
public class HistoricoPrecoNotFoundException extends RuntimeException{
    
    public HistoricoPrecoNotFoundException(Long id){
        super("Historico Preço "+id+" não encontrado.");
    }
    
    public HistoricoPrecoNotFoundException(){
        super("Histórico de Preço não encontrado.");
    }
}
