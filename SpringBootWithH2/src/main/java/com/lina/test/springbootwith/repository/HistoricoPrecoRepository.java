/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.repository;

import com.lina.test.springbootwith.domain.HistoricoPreco;
import com.lina.test.springbootwith.domain.PrecoVendaGroupingByMunicipio;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.lina.test.springbootwith.domain.HistoricoPrecoGroupingByMunicipio;
import com.lina.test.springbootwith.domain.HistoricoPrecoGroupingByBandeira;

/**
 *
 * @author Lina
 */
@Repository
public interface HistoricoPrecoRepository extends JpaRepository<HistoricoPreco, Long> {

    @Query("SELECT h FROM HistoricoPreco h WHERE LOWER(h.produto) LIKE LOWER(CONCAT('%', CONCAT(?1,'%')))")
    public List<HistoricoPreco> findLikeName(String produto);

    @Query(value = "SELECT M.NOME  AS municipio, round(avg(h.VALOR_VENDA),4) AS valorVenda "
            + "FROM TBL_HISTORICO_PRECO  H, TBL_REVENDA  R, TBL_MUNICIPIO M "
            + "WHERE H.REVENDA_ID = R.ID AND R.MUNICIPIO_ID = M.ID  and LOWER(M.NOME) = LOWER(?1) "
            + "GROUP BY M.ID "
            + "ORDER BY M.ID", nativeQuery = true)
    public PrecoVendaGroupingByMunicipio avgPrecoVendaByMunicipio(String id);

    @Query("SELECT h "
            + "FROM HistoricoPreco h "
            + "WHERE LOWER (h.revenda.municipio.regiaoSigla) = :regiao ")
    public List<HistoricoPreco> findByRegiao(String regiao);

    @Query("SELECT h "
            + "FROM HistoricoPreco h "
            + "WHERE LOWER (h.revenda.nome) = LOWER(:revenda) ")
    public List<HistoricoPreco> findByRevenda(String revenda);

    @Query("SELECT h "
            + "FROM HistoricoPreco h "
            + "WHERE h.dataColeta = :dataColeta ")
    public Optional<List<HistoricoPreco>> findByDataColeta(LocalDate dataColeta);

    @Query(value = "SELECT M.NOME  AS municipio, round(avg(h.VALOR_COMPRA),4) AS valorCompra, round(avg(h.VALOR_VENDA),4) AS valorVenda "
            + "FROM TBL_HISTORICO_PRECO  H, TBL_REVENDA  R, TBL_MUNICIPIO M "
            + "WHERE H.REVENDA_ID = R.ID AND R.MUNICIPIO_ID = M.ID "
            + "GROUP BY M.ID "
            + "ORDER BY M.ID", nativeQuery = true)
    public List<HistoricoPrecoGroupingByMunicipio> findGroupByMunicipio();

    @Query(value = "SELECT R.BANDEIRA  AS bandeira, round(avg(h.VALOR_COMPRA),4) AS valorCompra, round(avg(h.VALOR_VENDA),4) AS valorVenda "
            + "FROM TBL_HISTORICO_PRECO  H, TBL_REVENDA  R, TBL_MUNICIPIO M "
            + "WHERE H.REVENDA_ID = R.ID AND R.MUNICIPIO_ID = M.ID "
            + "GROUP BY R.BANDEIRA "
            + "ORDER BY R.BANDEIRA", nativeQuery = true)
    public List<HistoricoPrecoGroupingByBandeira> findGroupByBandeira();

}
