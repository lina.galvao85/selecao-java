/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Lina
 */
@Data
@Entity
@Table(name = "TBL_REVENDA")
public class Revenda implements Serializable{
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    private String nome;
    
    @Column(name="cod_instalacao")
    private int codInstalacao;
    
    private String bandeira; 
    
    @ManyToOne
    @JoinColumn(name="municipio_id", referencedColumnName="id")
    private Municipio municipio;

    public Revenda() {
    }

    public Revenda(int id, String nome, int codInstalacao, String bandeira, Municipio municipio) {
        this.id = id;
        this.nome = nome;
        this.codInstalacao = codInstalacao;
        this.bandeira = bandeira;
        this.municipio = municipio;
    }

}
