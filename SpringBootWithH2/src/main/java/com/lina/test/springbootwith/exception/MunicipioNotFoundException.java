/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.exception;

/**
 *
 * @author Lina
 */
public class MunicipioNotFoundException extends RuntimeException {
    
    public MunicipioNotFoundException (Long id){
        super("Municipio " + id + " não encontrado.");
    }
    public MunicipioNotFoundException (){
        super("Municipio não encontrado.");
    }
}
