/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.controller;

import com.lina.test.springbootwith.domain.HistoricoPreco;
import com.lina.test.springbootwith.domain.PrecoVendaGroupingByMunicipio;
import com.lina.test.springbootwith.exception.HistoricoPrecoNotFoundException;
import com.lina.test.springbootwith.exception.RevendaNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lina.test.springbootwith.repository.HistoricoPrecoRepository;
import com.lina.test.springbootwith.repository.RevendaRepository;
import io.swagger.annotations.ApiOperation;
import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.lina.test.springbootwith.domain.HistoricoPrecoGroupingByMunicipio;
import com.lina.test.springbootwith.domain.HistoricoPrecoGroupingByBandeira;

/**
 *
 * @author Lina
 */
@RestController
@RequestMapping("/rest/historicoPreco")
public class HistoricoPrecoController {

    @Autowired
    private HistoricoPrecoRepository repository;
    @Autowired
    private RevendaRepository revRepository;

    @GetMapping
    @ApiOperation(value="Pesquisa por todos os dados",
            notes="O serviço retorna a lista de todo o histórico de preço.")
    public List<HistoricoPreco> listAll() {
        return repository.findAll();
    }

    @GetMapping("/{produto}")
    @ApiOperation(value="Pesquisa por parte do nome do produto",
            notes="O serviço retorna a lista do histórico de preço pesquisado por nome do produto")
    public List<HistoricoPreco> listLikeNome(@PathVariable String produto) {
        return repository.findLikeName(produto);
    }

    @PostMapping
    @ApiOperation(value="Adiciona histórico de preço",
            notes="O serviço inclui novo histórico de preço.")
    public ResponseEntity<HistoricoPreco> save(@Valid
            @RequestBody HistoricoPreco histPreco) {
        int revId = histPreco.getRevenda().getId();
        histPreco.setRevenda(
                revRepository.findById(revId)
                        .orElseThrow(() -> new RevendaNotFoundException(revId))
        );
        return new ResponseEntity<>(repository.save(histPreco), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value="Update histórico de preço",
            notes="O serviço atualiza histórico de preço pesquisado com base no id.")
    public HistoricoPreco update(@Valid
        @RequestBody HistoricoPreco hstPrec, @PathVariable Long id) {

        return repository.findById(id).map(histPreco -> {
            histPreco.setProduto(hstPrec.getProduto());
            histPreco.setDataColeta(hstPrec.getDataColeta());
            histPreco.setValorCompra(hstPrec.getValorCompra());
            histPreco.setValorVenda(hstPrec.getValorVenda());
            histPreco.setUnidMedida(hstPrec.getUnidMedida());
            histPreco.getRevenda().setId(hstPrec.getRevenda().getId());

            return repository.save(histPreco);
        }).orElseThrow(()->new HistoricoPrecoNotFoundException(id) );
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="Deleta histórico de preço",
            notes="O serviço remove histórico de preço com base no id.")
    public void delete(@PathVariable Long id) {
        repository.findById(id).orElseThrow(() -> new HistoricoPrecoNotFoundException(id));
        repository.deleteById(id);
    }

    @GetMapping("/avgByMunicipioName/{nome}")
    @ApiOperation(value="Média de preço de venda por município",
            notes="O serviço retorna a média de preço de combustível com base no nome do município.")
    public PrecoVendaGroupingByMunicipio avgPrecoVendaByMunicipio(@PathVariable String nome) {
        return repository.avgPrecoVendaByMunicipio(nome);
    }

    @GetMapping("/findByRegiao/{regiao}")
    @ApiOperation(value="Informações por região",
            notes="O serviço retorna todas as informações importadas por sigla da região.")
    public List<HistoricoPreco> findByRegiao(@PathVariable String regiao) {
        return repository.findByRegiao(regiao);
    }

    @GetMapping("/findByRevenda/{revenda}")
    @ApiOperation(value="Informações por distribuidora",
            notes="O serviço retorna os dados agrupados por distribuidora.")
    public List<HistoricoPreco> findByRevenda(@PathVariable String revenda) {
        return repository.findByRevenda(revenda);
    }

    @GetMapping("/findByDataColeta/{dataColeta}")
    @ApiOperation(value="Informações por data da coleta",
            notes="O serviço retorna os dados agrupados pela data da coleta informada.")
    public List<HistoricoPreco> findByDataColeta(@PathVariable
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataColeta) {
        return repository.findByDataColeta(dataColeta).orElseThrow(() -> new HistoricoPrecoNotFoundException());
    }

    @GetMapping("/findGroupByMunicipio")
    @ApiOperation(value="Informações por município",
            notes="O serviço retorna o valor médio do valor da compra e do valor da venda por município.")
    public List<HistoricoPrecoGroupingByMunicipio> findGroupByMunicipio() {
        return  repository.findGroupByMunicipio();
    }

    @GetMapping("/findGroupByBandeira")
    @ApiOperation(value="Informações por bandeira",
            notes="O serviço retorna o valor médio do valor da compra e do valor da venda por bandeira.")
    public List<HistoricoPrecoGroupingByBandeira> findGroupByBandeira() {
        return  repository.findGroupByBandeira();
    }
    
}
