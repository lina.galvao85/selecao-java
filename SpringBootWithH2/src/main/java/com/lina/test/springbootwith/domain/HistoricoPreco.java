/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Lina
 */
@Data
@Entity
@Table(name="TBL_HISTORICO_PRECO")
public class HistoricoPreco implements Serializable{
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotEmpty(message = "Informe o produto")
    @Column
    private String produto;
    
    @NotNull(message = "Informe a data de coleta")
    @Column(name="data_coleta")
    private LocalDate dataColeta;
    
    @Column(name="valor_compra")
    private BigDecimal valorCompra;
    
    @Column(name="valor_venda")
    private BigDecimal valorVenda;
    
    @NotEmpty(message = "Informe a unidade de medida")
    @Column(name="unid_medida")
    private String unidMedida;
    
    @NotNull(message = "Informe a revenda")
    @ManyToOne
    @JoinColumn(name="revenda_id", referencedColumnName="id", nullable=false)
    private Revenda revenda;

    public HistoricoPreco() {
    }

    public HistoricoPreco(Long id, String produto, LocalDate dataColeta, BigDecimal valorCompra, BigDecimal valorVenda, String unidMedida, Revenda revenda) {
        this.id = id;
        this.produto = produto;
        this.dataColeta = dataColeta;
        this.valorCompra = valorCompra;
        this.valorVenda = valorVenda;
        this.unidMedida = unidMedida;
        this.revenda = revenda;
    }
    
}
