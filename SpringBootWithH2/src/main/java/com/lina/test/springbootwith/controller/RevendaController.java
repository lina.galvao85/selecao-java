/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.controller;

import com.lina.test.springbootwith.domain.Revenda;
import com.lina.test.springbootwith.repository.RevendaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lina
 */
@RestController
@RequestMapping("/rest/revenda")
public class RevendaController {
    
    @Autowired
    private RevendaRepository repository;
    
    @GetMapping
    public List<Revenda> listAll(){
        return repository.findAll();
    }
    
    @GetMapping("/{nome}")
    public List<Revenda> listLikeNome(@PathVariable String nome){
        return repository.findLikeName(nome);
    }
    
}
