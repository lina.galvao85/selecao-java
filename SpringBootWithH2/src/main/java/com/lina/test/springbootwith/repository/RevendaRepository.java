/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.repository;

import com.lina.test.springbootwith.domain.Revenda;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Lina
 */
public interface RevendaRepository extends JpaRepository<Revenda, Long> {
    @Query("Select r from Revenda r where LOWER(r.nome) LIKE LOWER(concat('%', concat(?1, '%')))")
    public List<Revenda> findLikeName(String nome);
    
    public Optional<Revenda> findById(int id);

    @Query("SELECT r FROM Revenda r WHERE LOWER (r.nome) = LOWER(?1)")
    public Optional<List<Revenda>> findByRevenda(String revenda);
}
