/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.repository;

import com.lina.test.springbootwith.domain.Municipio;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Lina
 */
public interface MunicipioRepository extends JpaRepository<Municipio,Long>{
    @Query("SELECT m FROM Municipio m WHERE LOWER(m.nome) LIKE LOWER(concat('%', concat(?1, '%')))")
    public List<Municipio> findLikeName(String nome);
    
    @Query("SELECT m FROM Municipio m WHERE LOWER (m.nome) = LOWER(?1)")
    public Optional<Municipio> findByName(String nome);
    
    @Query("SELECT m FROM Municipio m WHERE LOWER (m.regiaoSigla) = LOWER(?1)")
    public List<Municipio> findByRegiao(String regiao);
    
}