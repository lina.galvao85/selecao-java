/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

/**
 *
 * @author Lina
 */
@Data
@Entity
@Table(name = "TBL_MUNICIPIO")
public class Municipio implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotEmpty(message = "Informe a sigla da região")
    @Column(name="regiao_sigla")
    private String regiaoSigla;
    
    @NotEmpty(message = "Informe a sigla do estado")
    @Column(name="estado_sigla")
    private String estadoSigla;
    
    @NotEmpty(message = "Informe nome do município")
    @Column
    private String nome;

    public Municipio() {
    }

    public Municipio(Long id, String regiaoSigla, String estadoSigla, String municipio) {
        this.id = id;
        this.regiaoSigla = regiaoSigla;
        this.estadoSigla = estadoSigla;
        this.nome = municipio;
    }

    
}
