/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.exception;

/**
 *
 * @author Lina
 */
public class RevendaNotFoundException extends RuntimeException{

    public RevendaNotFoundException(int id) {
        super("Revenda id "+id+" não encontrada.");
    }
    
    public RevendaNotFoundException(){
        super("Revenda não encontrada.");
    }
    
}
