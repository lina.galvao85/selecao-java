/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lina.test.springbootwith.controller;

import com.lina.test.springbootwith.domain.Municipio;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lina.test.springbootwith.repository.MunicipioRepository;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author Lina
 */
@RestController
@RequestMapping("/rest/municipio")
public class MunicipioController {
    @Autowired
    private MunicipioRepository repository;
    
    @GetMapping("/{nome}")
    public List<Municipio> findLikeNome(@PathVariable String nome){
        return repository.findLikeName(nome);
    }
    @GetMapping
    public List<Municipio> findAll(){
        return repository.findAll();
    } 
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public Municipio create(@Valid @RequestBody Municipio municipio){
        return repository.save(municipio);
    } 
}
