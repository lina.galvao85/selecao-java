/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lina
 * Created: Apr 23, 2020
 */

DROP TABLE IF EXISTS TBL_MUNICIPIO;
CREATE TABLE TBL_MUNICIPIO (
    id INT AUTO_INCREMENT PRIMARY KEY,
    regiao_sigla VARCHAR(2) NOT NULL,
    estado_sigla VARCHAR(2) NOT NULL,
    nome VARCHAR(250) NOT NULL
);

DROP TABLE IF EXISTS TBL_REVENDA;
CREATE TABLE TBL_REVENDA(
    id INT AUTO_INCREMENT PRIMARY KEY, 
    nome VARCHAR(250) NOT NULL, 
    cod_instalacao INT NOT NULL, 
    bandeira VARCHAR(150) NOT NULL, 
    municipio_id INT NOT NULL,
    foreign key (municipio_id) references TBL_MUNICIPIO(id)
);

DROP TABLE IF EXISTS TBL_HISTORICO_PRECO;
CREATE TABLE TBL_HISTORICO_PRECO(
    id INT AUTO_INCREMENT PRIMARY KEY,
    revenda_id INT NOT NULL, 
    produto VARCHAR(100) NOT NULL,
    data_coleta DATE NOT NULL,
    valor_compra decimal,
    valor_venda decimal, 
    unid_medida VARCHAR(100) NOT NULL,
    foreign key (revenda_id) references TBL_REVENDA(id)
);
